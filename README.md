# AutoFilter for Raspberry Pi 4
This program was designed for use with a Raspberry Pi 4 controller board with GPIO pins connected as required for relay operation.

## Prerequasites
- A Raspberry Pi 4 with a usable Linux Operating System
- Git
- jq

## Usage
To get the program onto the Raspberry Pi 4, clone this repository by issuing the following command on a terminal
`git clone https://gitlab.com/BlacKernel/autofilter.git`

Then enter the cloned repository by issuing the command
`cd autofilter`

Then you may run the program by issuing the command as root
`sudo ./auto.sh`

# Structure of the Program JSON
TBA
