#!/bin/bash

# Import GPIO Library
. "$PWD/lib/lib_gpio.sh"

# Global Variables
declare -A pinArray		# Array to store name-pin pairs
PROGRAM=ultrafilter1.json	# Program to use for this application
COUNT=0				# Timer variable
MODE=				# Mode that the application is currently in (init, function, cycle, deinit)
STATUS=				# Status of the current cylcle or function
FUNC_LOCK=0			# Locking flag for functions
CYCLE_LOCK=0			# Locking flag for cycles


# Function to initialize the session
function init() {
	# Initialize the realational array to map pins to names and visa versa
	echo "Initializing pin array..."
	
	for i in $(seq 1 $(jq ".pins | length" "./programs/$PROGRAM"));do
		pinArray[$(jq ".pins[$(($i-1))].name" "./programs/$PROGRAM" | tr -d '"' )]=$(jq ".pins[$(($i-1))].pin" "./programs/$PROGRAM")
	done

	# Export all pins given in the program
	echo "Exporting all pins..."
	
	for i in ${pinArray[@]};do
		exportPin $i
	done

	# Set all pins directionality
	echo "Setting pin directionality..."
	
	for i in $(seq 1 $(jq ".pins | length" "./programs/$PROGRAM"));do
		setDir "$(jq ".pins[$(($i-1))].pin" "./programs/$PROGRAM")" "$(jq ".pins[$(($i-1))].dir" "./programs/$PROGRAM" | tr -d '"' )"
	done

	# Set default value for all pins
	echo "Setting default values..."
	
	for i in $(seq 1 $(jq ".pins | length" "./programs/$PROGRAM"));do
		setVal "$(jq ".pins[$(($i-1))].pin" "./programs/$PROGRAM")" "$(jq ".pins[$(($i-1))].value" "./programs/$PROGRAM")"
	done
	clear
	
	# Run the initialization function for the program
	MODE="init"
	funcRun "$(jq ".init" "./programs/$PROGRAM" | tr -d '"')"
}

# Main function loop
function main() {
	while [ 1 ];do
		printMenu
		read -p "Command: "
		if	[ "$(echo "$REPLY" | cut -d' ' -f1)" == "function" ];then
			MODE="function"
			funcRun $(echo "$REPLY" | cut -d' ' -f2-)
		elif	[ "$(echo "$REPLY" | cut -d' ' -f1)" == "cycle" ];then
			MODE="cycle"
			cycleRun $(echo "$REPLY" | cut -d' ' -f2-)
		elif	[ "$(echo "$REPLY" | cut -d' ' -f1)" == "exit" ];then
			deinit
		else
			printHelp "$REPLY"
		fi
	done
}

# Function to safely shutdown the system
function deinit() {
	# Make sure count is set to zero immediately
	COUNT=0

	# Make sure the locks are set to zero
	FUNC_LOCK=0
	CYCLE_LOCK=0

	# Run the deinitialization function for the program
	MODE=deinit
	funcRun "$(jq ".deinit" "./programs/$PROGRAM" | tr -d '"')"

	# Exit gracefully
	exit 0
}

# Print the menu before the command prompt
function printMenu() {
	clear
	echo "Mode: $MODE"
	echo "Status: $STATUS"
	echo "Current Timer: $COUNT"
	echo "Name - Pin - Value"
	for i in ${!pinArray[@]};do
		echo "$i - ${pinArray[$i]} - $(getVal ${pinArray[$i]})"
	done | sort -n -k2
}

# Print the help information
function printHelp() {
	clear
	echo "Ultrafilter Bench Automation Program"
	echo "------------------------------------"
	echo "If you would like to run a function 'foo',
enter the following at the command prompt:

function foo

If you would like to run a cycle of functions 'bar' twice,
entter the following at the command prompt:

cycle bar 2

The syntax is 'function nameOfFunction' or 'cycle nameOfCycle numberOfCycles'"

	MODE='help'
	STATUS="'$@' is not a valid command"
	read -p "Press any key to continue..." -n 1
}

# usage: funcRun $1
# Runs the function, $1, from the program
function funcRun() {
	func=$1

	if [ "$(jq ".functions.$func" "./programs/$PROGRAM")" == "null" ];then
		STATUS="$func is not a valid function"
		main
	else
		stepIndex=1
		totalSteps="$(jq ".functions.$func | length" "./programs/$PROGRAM")"
		FUNC_LOCK=1
		if [ $CYCLE_LOCK == 0 ];then
			STATUS="$func, $stepIndex/$totalSteps"
		else
			STATUS="$func, $stepIndex/$totalSteps ($CYC, $cycleIndex/$totalCycles)"
		fi
	fi

	while [ $FUNC_LOCK == 1 ];do
		if [ $COUNT == 0 ];then
			setVal "${pinArray[$(jq ".functions.$func[$(($stepIndex-1))].name" "./programs/$PROGRAM" | tr -d '"')]}" "$(jq ".functions.$func[$(($stepIndex-1))].value" "./programs/$PROGRAM")"
			COUNT="$(jq ".functions.$func[$(($stepIndex-1))].hold" "./programs/$PROGRAM")"
		elif (( $COUNT > 0 ));then
			sleep 1
			COUNT=$(($COUNT-1))
		fi
		printMenu
		if [ $stepIndex == $totalSteps -a $COUNT == 0 ];then
			FUNC_LOCK=0
			break
		elif [ $COUNT == 0 ];then
			stepIndex=$(($stepIndex+1))
			if [ $CYCLE_LOCK == 0 ];then
				STATUS="$func, $stepIndex/$totalSteps"
			else
				STATUS="$func, $stepIndex/$totalSteps ($CYC, $cycleIndex/$totalCycles)"
			fi
		fi
	done
}

# usage: cycleRun $1 $2
# Runs the cycle, $1, a certain number of times, $2
function cycleRun() {
	CYC=$1
	totalCycles=$2

	if [ "$(jq ".cycles.$CYC" "./programs/$PROGRAM")" == "null" ];then
		STATUS="$CYC is not a valid cycle"
		main
	else
		cycleIndex=1
		CYCLE_LOCK=1
	fi

	while [ $CYCLE_LOCK == 1 ];do
		for i in $(jq ".cycles.$CYC[].function" "./programs/$PROGRAM");do
			funcRun $(echo $i | tr -d '"')
		done
		if [ $cycleIndex == $totalCycles ];then
			break
		else
			cycleIndex=$(($cycleIndex+1))
		fi
	done
}

if [ "$EUID" -ne 0 ];then
	echo "Please run this program as root with the command 'sudo ./auto.sh'"
	exit 1
fi

trap deinit SIGINT

init
main
