#!/bin/bash

#	GPIO Bash Library
#	By: Jacob Rogers
#	Released under the GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

# Base path for GPIO pins
BASE_GPIO_PATH=/sys/class/gpio

# Export a pin to the system so that it can be used
function exportPin() {
	if [ ! -e $BASE_GPIO_PATH/gpio$1 ];then
		echo "$1" > $BASE_GPIO_PATH/export
	fi
}

# Set the direction state of the pin
function setDir() {
	if [ "$2" == "in" ];then
		echo "in" > $BASE_GPIO_PATH/gpio$1/direction
	elif [ "$2" == "out" ];then
		echo "out" > $BASE_GPIO_PATH/gpio$1/direction
	else
		echo "$2 is not a valid direction state."
		exit 1
	fi
}

# Set the value of the pin
function setVal() {
	if [ "$2" == "1" ];then
		echo "1" > $BASE_GPIO_PATH/gpio$1/value
	elif [ "$2" == "0" ];then
		echo "0" > $BASE_GPIO_PATH/gpio$1/value
	else
		echo "$2 is not a valid value state."
		exit 1
	fi
}

# Get the direction state of the pin
function getDir() {
	cat $BASE_GPIO_PATH/gpio$1/direction
}

# Get the value of the pin
function getVal() {
	cat $BASE_GPIO_PATH/gpio$1/value
}
